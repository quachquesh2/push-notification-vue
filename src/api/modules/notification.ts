import { axios } from '../index.config'

export const notificationApi = {
  subscribe: (data: any) => axios.post('/auth/subscription-user', data)
}
